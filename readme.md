<!--
- SPDX-License-Identifier: CC-BY-SA-4.0
- SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
-->

# Simple Reproducer for Catch2 with vcpkg


## Install vcpkg

1.Linux

```bash
git clone https://github.com/Microsoft/vcpkg
cd vcpkg
./bootstrap-vcpkg.sh
./vcpkg integrate install
cd ..
```

2. Windows

```powershell
git clone https://github.com/Microsoft/vcpkg
cd vcpkg
bootstrap-vcpkg.bat
vcpkg integrate install
cd ..
```


## Compile and Run the MVE

```bash
git clone https://gitlab.com/sutarwadi/catch2-vcpkg.git code
cd code
cmake -S . -B ../build --toolchain ../vcpkg/scripts/buildsystems/vcpkg.cmake
cmake --build ../build
ctest --test-dir ../build
```


