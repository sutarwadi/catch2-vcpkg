// -------------------------------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
// -------------------------------------------------------------------------------------------------

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Dummy test", "[dummy]") { REQUIRE(true); }
