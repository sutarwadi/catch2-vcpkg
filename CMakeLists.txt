# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

cmake_minimum_required(VERSION 3.26)
project(catch2-vcpkg)
include(CTest)

find_package(Catch2 REQUIRED)
add_executable(main main.cpp)
target_link_libraries(main PRIVATE Catch2::Catch2WithMain)
add_test(NAME main COMMAND main)
